#!/usr/bin/perl

use Glib;
use Gtk2;
use Gtk2::Gdk::Keysyms;

sub filter_keypress {
  my ($imobj, $event) = @_;

  if( $event->keyval == $Gtk2::Gdk::Keysyms{ Return } ) {
  #  $imobj->signal_emit("preedit_changed");
  #  print ": Return\n";
  } else {
    $ch = sprintf("%c", Gtk2::Gdk->keyval_to_unicode($event->keyval));
    $imobj->signal_emit("commit", $ch);
    return TRUE;
  }

  $geo = Gtk2::Gdk::Geometry -> new;

  return $geo;
}

sub get_preedit_string {
  $str = "ahoaho";
  return ($str, "attr", length($str));
}
