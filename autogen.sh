#!/bin/sh

# glib-gettextize --copy --force
aclocal
libtoolize --copy --force
aclocal
automake -c -a
autoconf

./configure $@
