/* Perl binding for GTK+ immodule
 *
 * Copyright (C) 2000 Red Hat Software
 * Copyright (C) 2002 Yusuke Tabata
 * Copyright (C) 2004 Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Authors: Owen Taylor <otaylor@redhat.com>
 *          Yusuke Tabata <tee@kuis.kyoto-u.ac.jp>
 *          Yukihiro Nakai <ynakai@redhat.com>
 *
 */

#include <EXTERN.h>
#include <perl.h>
#include <gperl.h>
#include <gtk2perl.h>

#include <string.h>

#include <gtk/gtk.h>
#include <gtk/gtkimmodule.h>

#include <gdk/gdkkeysyms.h>

#define IM_CONTEXT_PERL(obj) (GTK_CHECK_CAST((obj), type_perl, IMContextPerl))
#ifdef BUFSIZ
#undef BUFSIZ
#define BUFSIZ 1024
#endif

static PerlInterpreter *my_perl;

extern void xs_init(void);

typedef struct _IMContextPerl {
  GtkIMContext parent;
} IMContextPerl;

typedef struct _IMContextPerlClass {
  GtkIMContextClass parent_class;
} IMContextPerlClass;

GType type_perl = 0;

static void im_perl_class_init (GtkIMContextClass *class);
static void im_perl_init (GtkIMContext *im_context);
static void im_perl_finalize(GObject *obj);
static gboolean im_perl_filter_keypress(GtkIMContext *context,
		                GdkEventKey *key);
static void im_perl_get_preedit_string(GtkIMContext *, gchar **str,
					PangoAttrList **attrs,
					gint *cursor_pos);

static void
im_perl_register_type (GTypeModule *module)
{
  static const GTypeInfo object_info =
  {
    sizeof (IMContextPerlClass),
    (GBaseInitFunc) NULL,
    (GBaseFinalizeFunc) NULL,
    (GClassInitFunc) im_perl_class_init,
    NULL,           /* class_finalize */
    NULL,           /* class_data */
    sizeof (IMContextPerl),
    0,
    (GtkObjectInitFunc) im_perl_init,
  };

  type_perl = 
    g_type_module_register_type (module,
				 GTK_TYPE_IM_CONTEXT,
				 "IMContextPerl",
				 &object_info, 0);
}

static void
im_perl_class_init (GtkIMContextClass *class)
{
  GtkIMContextClass *im_context_class = GTK_IM_CONTEXT_CLASS(class);
  GObjectClass *object_class = G_OBJECT_CLASS(class);

  //im_context_class->set_client_window = im_perl_set_client_window;
  im_context_class->filter_keypress = im_perl_filter_keypress;
  im_context_class->get_preedit_string = im_perl_get_preedit_string;

  object_class->finalize = im_perl_finalize;
}

static void
im_perl_init (GtkIMContext *im_context)
{
  char *script_args[] = { "", "immodule.pl", NULL };
  char *args[] = { NULL };
  my_perl = perl_alloc();
  perl_construct(my_perl);
  PL_exit_flags |= PERL_EXIT_DESTRUCT_END;
  perl_parse(my_perl, xs_init, 2, script_args, (char **)NULL);
  perl_run(my_perl);
}

static const GtkIMContextInfo im_perl_info = { 
  "Perl",		   /* ID */
  "Perl",             /* Human readable name */
  "gtk+",			   /* Translation domain */
   "",		   /* Dir for bindtextdomain (not strictly needed for "gtk+") */
   ""			           /* Languages for which this module is the default */
};

static const GtkIMContextInfo *info_list[] = {
  &im_perl_info
};

void
im_module_init (GTypeModule *module)
{
  im_perl_register_type(module);
}

void 
im_module_exit (void)
{
}

void 
im_module_list (const GtkIMContextInfo ***contexts,
		int                      *n_contexts)
{
  *contexts = info_list;
  *n_contexts = G_N_ELEMENTS (info_list);
}

GtkIMContext *
im_module_create (const gchar *context_id)
{
  if (strcmp (context_id, "Perl") == 0)
    return GTK_IM_CONTEXT (g_object_new (type_perl, NULL));
  else
    return NULL;
}

gboolean
im_perl_filter_keypress(GtkIMContext *context, GdkEventKey *key)
{
  if( key->type == GDK_KEY_RELEASE )
    return FALSE;

  dSP;
  int count;

  ENTER;
  SAVETMPS;
  PUSHMARK(SP);
  XPUSHs(sv_2mortal(newSVGObject(G_OBJECT(context))));
  XPUSHs(sv_2mortal(newSVGdkEvent(key)));
  PUTBACK;
  count = perl_call_pv("filter_keypress", G_SCALAR);
  SPAGAIN;

  gboolean ret = (gboolean)POPi;

  PUTBACK;
  FREETMPS;
  LEAVE;

  return ret;
}

void
im_perl_get_preedit_string(GtkIMContext *ic, gchar **str,
			    PangoAttrList **attrs, gint *cursor_pos)
{
  int count;
  IMContextPerl *ec = IM_CONTEXT_PERL(ic);

  if (attrs) {
    *attrs = pango_attr_list_new();
  }
  if (cursor_pos) {
    *cursor_pos = 0;
  }

  dSP;
  STRLEN n_a;

  ENTER;
  SAVETMPS;
  PUSHMARK(SP);
  PUTBACK;
  count = perl_call_pv("get_preedit_string", G_ARRAY | G_SCALAR);
  SPAGAIN;

  int cpos = POPi;
  SV* attrs_pop = POPs;
  char* str_pop = POPpx;

  g_print("str_sv: %s\n", str_pop);
  //g_print("attrs_sv: %s\n", attrs_sv);
  g_print("cpos: %d\n", cpos);

  *str = g_strdup(str_pop);

  PUTBACK;
  FREETMPS;
  LEAVE;

  if( cursor_pos ) {
    *cursor_pos = cpos;
  }

  if( *str == NULL || strlen(*str) == 0 ) return;

}

static void
im_perl_finalize(GObject *obj) {
  IMContextPerl* ec = IM_CONTEXT_PERL(obj);

  perl_destruct(my_perl);
  perl_free(my_perl);
}
